;;   Copyright (c) Dragan Djuric. All rights reserved.
;;   The use and distribution terms for this software are covered by the
;;   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php) or later
;;   which can be found in the file LICENSE at the root of this distribution.
;;   By using this software in any fashion, you are agreeing to be bound by
;;   the terms of this license.
;;   You must not remove this notice, or any other, from this software.

(defproject minimal-dd-lein "0.0.1"
  :description "minimal setup for deep diamond"
  :author "henrik"
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [uncomplicate/neanderthal "0.40.0"]
                 [org.bytedeco/dnnl-platform "2.1.1-1.5.5"]
                 [org.jcuda/jcudnn "11.1.1"]
                 [uncomplicate/deep-diamond "0.20.2"]
                 ]

  :profiles {:dev {:plugins [[lein-midje "3.2.1"]
                             [lein-codox "0.10.6"]]
                   :resource-paths ["data"]
                   :global-vars {*warn-on-reflection* false
                                 *assert* false
                               ;;  *unchecked-math* :warn-on-boxed
                                 *print-length* 128}
                   :dependencies [[midje "1.9.10"]
                                  [org.clojure/data.csv "1.0.0"]]}}

  :jvm-opts ^:replace ["-Dclojure.compiler.direct-linking=true" "-XX:+UseLargePages"
                       "--add-opens=java.base/jdk.internal.ref=ALL-UNNAMED"]

  :javac-options ["-target" "1.8" "-source" "1.8" "-Xlint:-options"]
  :source-paths ["src/clojure" "src/device"])
