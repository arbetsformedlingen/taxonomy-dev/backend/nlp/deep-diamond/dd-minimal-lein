(ns clojure.core
  (:require

   [uncomplicate.diamond.internal.protocols
    :refer [diff-weights forward backward layers diff-input diff-output
            weights bias *workspace* inf-ws-size train-ws-size create-workspace]]

   [uncomplicate.neanderthal
    [core :refer [entry! entry native transfer! view-vctr vctr
                  cols view-ge nrm2 axpy!]]
    [random :refer [rand-uniform!]]
    [math :as math]
    [vect-math :refer [div!]]]

   [uncomplicate.commons.core :refer [with-release]]
   [uncomplicate.diamond.dnn :refer :all]
   [uncomplicate.diamond.tensor :refer :all]

   [uncomplicate.neanderthal.native :refer [fge]]
   [uncomplicate.diamond.metrics :refer [classification-metrics confusion-matrix]]
   ))



(defn generate-test-data [number-of-examples]
  (fge 4 number-of-examples
       (take number-of-examples (repeatedly #(vec '(0 0 0 1)))))
  )


#_(def x-train (fge 4 5 [[0 0 0 1]
                       [0 0 0 1]
                       [0 0 0 1]
                       [0 0 0 1]
                       [0 0 0 1]
                       ]))

#_(def y-train (fge 4 5 [[0 0 0 1]
                       [0 0 0 1]
                       [0 0 0 1]
                       [0 0 0 1]
                       [0 0 0 1]
                       ]))


#_(def x-test (fge 4 5 [[0 0 0 1]
                      [0 0 0 1]
                      [0 0 0 1]
                       [0 0 0 1]
                       [0 0 0 1]
                       ]))

#_(def y-test (fge 4 5 [[0 0 0 1]
                      [0 0 0 1]
                      [0 0 0 1]
                      [0 0 0 1]
                      [0 0 0 1]
                      ]))


;; (def x-train (generate-test-data 100))
;; (def x-test (generate-test-data 100))
;; (def y-train (generate-test-data 100))
;; (def y-test (generate-test-data 100))

;; words in vocabulary 4
;; number of training examples in batch 10
;; total number of training examples 100
;; total number of test examples 100

(defn word2vec [fact]
  (with-release [x-tensor           (tensor fact [100 4] :float :nc)
                 x-minibatch-tensor (tensor fact [10 4] :float :nc)
                 y-tensor           (tensor fact [100 4] :float :nc)
                 y-minibatch-tensor (tensor fact [10 4] :float :nc)
                 net-blueprint      (network fact x-minibatch-tensor
                                             [(fully-connected [128] :relu)
                                              (fully-connected [128] :relu)
                                              (fully-connected [192] :relu)
                                              (fully-connected [4] :softmax)  ;; 4 word
                                              ])
                 net (init! (net-blueprint x-minibatch-tensor :adam))
                 net-infer (net-blueprint x-minibatch-tensor)
                 crossentropy-cost (cost net y-minibatch-tensor :crossentropy)
                 x-batcher (batcher x-tensor (input net))
                 y-batcher (batcher y-tensor y-minibatch-tensor)
                 ]
    (transfer! x-train (view-vctr x-tensor))
    (transfer! y-train (view-vctr y-tensor))
    (time (train net x-batcher y-batcher crossentropy-cost 5 []))
    (transfer! net net-infer);; ?? why
    (with-release [x-test-tensor (tensor fact [100 4] :float :nc)
                   y-test-tensor (tensor fact [100 4] :float :nc)
                   _ (transfer! x-test (view-vctr x-test-tensor))
                   _ (transfer! y-test (view-vctr y-test-tensor))

                   infered-words (infer net-infer x-test-tensor)

                   ]
      [(classification-metrics y-test-tensor infered-words)
       (confusion-matrix y-test-tensor infered-words)
       ]
      )
    )
  )
